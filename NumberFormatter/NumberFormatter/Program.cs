﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NumberFormatter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please, enter values:");

            List<string> list = ReadDataFromConsole();

            PrintData(list, "Entered values:");

            list = FormatData(list);

            PrintData(list, "Formatted view:");

            Console.WriteLine(string.Empty);
            var path = Path.Combine(Environment.CurrentDirectory, "datasource.txt");
            list = ReadDataFromFile(path);

            PrintData(list, "Data from file:");

            list = FormatData(list);

            PrintData(list, "Formatted view:");
        }

        private static List<string> ReadDataFromConsole()
        {
            var list = new List<string>();

            var queue = new Queue<char>();
            while (true)
            {
                var code = Console.Read();
                try
                {
                    if (code < 0)
                    {
                        break;
                    }

                    var symbol = Convert.ToChar(code);
                    if (symbol == '\n')
                    {
                        var sb = new StringBuilder();
                        while (queue.Count > 0)
                        {
                            sb.Append(queue.Dequeue());
                        }

                        queue.Clear();

                        var s = sb.ToString().Trim();
                        if (s.Length == 0)
                        {
                            break;
                        }

                        list.Add(s);
                    }

                    queue.Enqueue(symbol);
                }
                catch (OverflowException e)
                {
                    Console.WriteLine($"Exception: {e.Message}");
                    throw;
                }
            }

            return list;
        }

        private static void PrintData(List<string> list, string header)
        {
            Console.WriteLine(header);

            foreach (var line in list)
            { 
                Console.WriteLine(line);
            }
        }

        private static List<string> FormatData(List<string> list)
        {
            var result = new List<string>();
            var sb = new StringBuilder();

            foreach (var line in list)
            {
                var pairs = line.Split(' ');
                
                foreach (var pair in pairs)
                {
                    var items = pair.Split(new[] { ',' });
                    if (items.Length == 2)
                    {
                        var x = items[0].Trim().Replace('.', ',');
                        var y = items[1].Trim().Replace('.', ',');
                        sb.Append($"X:{x}Y:{y} ");
                    }
                }
                
                result.Add(sb.ToString());
                sb.Clear();
            }

            return result;
        }

        private static List<string> ReadDataFromFile(string filePath)
        {
            var list = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string file;
                    while ((file = sr.ReadLine()) != null)
                    {
                        list.Add(file);
                    }
                    sr.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}");
                throw;
            }

            return list;
        }
    }
}